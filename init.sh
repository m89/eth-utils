#!/bin/bash
geth --identity "MyNodeName" \
     --rpc --rpccorsdomain "*" \
     --datadir "~/eth/private" \
     --nodiscover \
     --rpcapi "db,eth,net,web3" \
     --autodag \
     --networkid 1900 \
     --nat "any" \
     init CustomGenesis.json

geth --datadir "~/eth/private" account import ./privkey
geth --datadir "~/eth/private" account import ./privkey2
geth --datadir "~/eth/private" account import ./privkey3
geth --datadir "~/eth/private" account import ./privkey4
geth --datadir "~/eth/private" account import ./privkey5